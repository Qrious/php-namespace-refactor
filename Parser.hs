{- |
Module      :  Parser
Description :  The parsers used to parse the PHP Files, to
			   refactor namespace definitions
Copyright   :  (c) Sander Klock
License     :  http://opensource.org/licenses/mit-license.php

Maintainer  :  j.a.klock@students.uu.nl
Stability   :: Stable
Portability :: Portable 
-}
module Parser where

import ParseLib.Abstract
import Data.List(nub,delete)

-- Parses a entire file
parseFile needle replace xs = unlines $ map fst $ concat $ map (parse $ pLine needle replace) $ lines xs

-- Parses a single line
pLine :: String -> String -> Parser Char String
pLine needle replace =  (\y x ->flattenTuple $ head $  parse (pReplace needle replace) ( y++x++";") ) 
        <$> (token "namespace" <<|> token "use")
        <*> greedy (satisfy (\x -> x /= ';')) <* symbol ';'
        <<|> pGreedyAll

flattenTuple (x,y) = x ++ y
-- Does the actual replacing when the token is matched
pReplace :: String -> String -> Parser Char String
pReplace needle replace = ((\x y z -> x ++ replace ++ z) <$>
                            pAll <*> token needle <*> pGreedyAll)
                          <<|> pGreedyAll

-- Matches everything
pAll :: Parser Char String
pAll = many $ satisfy (\_ -> True)

-- Greedy variant of pAll
pGreedyAll :: Parser Char String
pGreedyAll = greedy $ satisfy (\_ -> True)