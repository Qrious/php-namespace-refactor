{- |
Module      :  Process
Description :  Replaces the namespace for each file
Copyright   :  (c) Sander Klock
License     :  http://opensource.org/licenses/mit-license.php

Maintainer  :  j.a.klock@students.uu.nl
Stability   :: Maybe Stable
Portability :: Maybe Portable 
-}
module Process where

import Parser
import System.IO
import System.Directory
import System.FilePath((</>))
import Control.Applicative((<$>))
import Control.Exception(throw)
import Control.Monad(when,forM_,forM)
import Data.List(isInfixOf)
import Control.DeepSeq (rnf)

-- Processes all the files
process filterext files needle replace folder = do putStrLn $ "Making backup of current code in [" ++ folder' folder ++"/.refactor/]"
                                                   copyDir filterext folder $ (folder' folder) ++ "/.refactor"
                                                   putStrLn "Backup completed"
                                                   mapM_ (processSingle needle replace) files
                                      where folder' f = if last f == '/'
                                                            then folder' $ init f
                                                            else f
-- Process a single file
processSingle needle replace file = do handle <- openFile file ReadMode
                                       hSetBuffering handle NoBuffering
                                       content <- hGetContents handle
                                       rnf content `seq` hClose handle
                                       putStrLn $ "Processing file " ++ show file
                                       let new = parseFile needle replace content
                                       writeFile file new

-- Copies all files that match the filter extension directory recursive
copyDir :: String -> FilePath -> FilePath -> IO ()
copyDir filterext src dst = do
  whenM (not <$> doesDirectoryExist src) $
    throw (userError "source does not exist")
  whenM (doesFileOrDirectoryExist dst) $
    removeDirectoryRecursive dst -- Remove .refactor backup if it exists

  createDirectory dst
  content <- getDirectoryContents src
  let xs = filter (`notElem` [".", "..",".refactor"]) content
  forM_ xs $ \name -> do
    let srcPath = src </> name
    let dstPath = dst </> name
    isDirectory <- doesDirectoryExist srcPath
    if isDirectory
      then copyDir filterext srcPath dstPath
      else if (isInfixOf filterext name)
                then copyFile srcPath dstPath
                else return ()

  where
    doesFileOrDirectoryExist x = orM [doesDirectoryExist x, doesFileExist x]
    orM xs = or <$> sequence xs
    whenM s r = s >>= flip when r

-- Finds all files in the folder Recursively matching the filter extension
getRecursiveContents :: String -> FilePath -> IO [FilePath]
getRecursiveContents filterext topdir = do
  names <- getDirectoryContents topdir
  let properNames = filter (`notElem` [".", "..",".refactor"]) names
  paths <- forM properNames $ \name -> do
    let path = topdir </> name
    isDirectory <- doesDirectoryExist path
    if isDirectory
      then getRecursiveContents filterext path
      else if isInfixOf filterext path 
            then return [path]
            else return []
  return (concat paths)