# About

Created by Sander Klock.

Used code snippets from: (All credits go to the original authors)
- http://stackoverflow.com/questions/6807025/what-is-the-haskell-way-to-copy-a-directory (By Oliver)
- http://book.realworldhaskell.org/read/io-case-study-a-library-for-searching-the-filesystem.html (By Bryan O'Sullivan, Don Stewart, and John Goerzen)

If the authors want their code removed, please contact me.
--------------------------------------------------------------------
Changelog:

#10-12-2012
- Added extra extension filter parameter (default: .php)
- Moved getRecursiveContents to Process.hs
- Added some comments



--------------------------------------------------------------------
# Additional notes

The Main executable distributed was compiled using the -threaded flag.
The code is quite sloppy sometimes, because I just needed this tool.
(Added some comments in the meantime).
However one can use it for development. I do not recommend using it on production servers.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------

# Running our Program

After the program has been compiled, it can be executed by typing "Main.exe srcdirectory oldnamespace newnamespace".
Our program has the following syntax: Main.exe [srcdirectory] [oldnamespace] [newnamespace] [filterext].
All arguments are optional, as the program prompts for them.

Detailed explanation:
- srcdirectory: The folder in which the PHP files reside that need to be refactored (Recursively looks into subfolders) (default: /var/www)
- oldnamespace: The (partial) namespace to replace (default: oldnamespace)
- newnamespace: The (partial) namespace to replace the old (default: newnamespace.
- filterext: The extension to filter for (default: .php)

--------------------------------------------------------------------
END OF README.TXT
