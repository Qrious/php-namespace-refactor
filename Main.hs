{- |
Module      :  Main
Description :  Bootstrap file.
Copyright   :  (c) Sander Klock
License     :  http://opensource.org/licenses/mit-license.php

Maintainer  :  j.a.klock@students.uu.nl
Stability   :: Maybe Stable
Portability :: Maybe Portable 
-}
module Main where

import System.IO
import Control.Exception
import System.Environment (getArgs)
import Data.Char(toLower)
import Data.List(isInfixOf)
import Control.Monad
import Parser
import Process

-- Main execution point
main =  initalise `Control.Exception.catch` excephandler

-- Initialise the program
initalise = do
       -- Set the stdout at no buffering, so putStr will be written immediately
       hSetBuffering stdout NoBuffering
       -- Read the commandline arguments
       [input,ns,nns,filterext] <- fmap (defaultArgs ["/var/www/","OldNamespace","NewNamespace",".php"]) getArgs
       putStrLn "#############################################"
       putStrLn "# Authors: Sander Klock                     #"
       putStrLn "# License: MIT-License                      #"
       putStrLn "# Version: 3.0.0                            #"
       putStrLn "#                                           #"
       putStrLn "#############################################"
       putStrLn ""
       putStrLn "Starting NamespaceRefactor..."
       putStrLn ""
       putStrLn "Please choose the directory to refactor      "
       putStrLn "the PHP namespaces for:                      "
       putStr $ " [" ++ show input ++ "] :"
       uinput <- getLine
       let filepath = handleinput uinput input
       putStrLn filepath
       files <- getRecursiveContents filterext filepath
       case length files of
        0 -> do putStrLn "No PHP files found to refactor..."
                restart
        otherwise -> do putStrLn $ "Found " ++ show (length files) ++ " PHP Files"
                        putStrLn "The (partial) namespace to find: "
                        putStr $ " [" ++ show ns ++ "] :"
                        uns <- getLine
                        let namespace = handleinput uns ns
                        putStrLn "The (partial) namespace to replace: "
                        putStr $ " [" ++ show nns ++ "] :"
                        unns <- getLine
                        let newnamespace = handleinput unns nns
                        putStrLn ""
                        putStrLn "We are going to:"
                        putStrLn $ "- Look in directory [" ++ input ++ "]"
                        putStrLn $ "- Filter for extension [" ++ filterext ++ "]"
                        putStrLn $ "- Move files in namespace [" ++ namespace ++ "]"
                        putStrLn $ "- To namespace [" ++ newnamespace ++ "]"
                        putStrLn "Are you sure (Y/N)?"
                        sure <- getLine
                        if stringToBool sure
                          then process filterext files namespace newnamespace input
                          else restart
-- Handle User input
handleinput x input  | null x = input
                     | otherwise = x

-- Function to merge default arguments with input arguments
defaultArgs :: [String] -> [String] -> [String]
defaultArgs [] _ = []
defaultArgs (d:def) [] = d : defaultArgs def []
defaultArgs (_:def) (a:args) = a : defaultArgs def args

-- Exception handling
excephandler :: IOException -> IO ()
excephandler e  = do putStrLn $ "  " ++ show e ++ "                 [ERROR]"
                     putStrLn "Aborting..."
                     putStrLn "Launching Nuclear Missiles to nuke all bugs!"
                     restart

-- Ask to restart the program
restart :: IO ()
restart = do putStrLn "Do you want to restart? (Y/N)"
             restart <- getLine
             if stringToBool restart
             then main
             else putStrLn "Exiting NamespaceRefactor , Goodbye"
             
                   
-- Interpret a string as a boolean
stringToBool :: String -> Bool
stringToBool str = map toLower str `elem` ["yes", "true", "y", "ja", "j"]