{- 
Module      :  RecursiveDirectoryContents
Description :  Returns a list of all files in a directory and its subdirectories
Copyright   :  (c) Sander Klock
License     :  http://opensource.org/licenses/mit-license.php

Maintainer  :  j.a.klock@students.uu.nl 
Stability   :: Stable
Portability :: Portable 
-}

module RecursiveContents (getRecursiveContents) where

import Control.Monad (forM)
import System.Directory (doesDirectoryExist, getDirectoryContents)
import System.FilePath ((</>))
import Data.List(isInfixOf)

getRecursiveContents :: FilePath -> IO [FilePath]

getRecursiveContents topdir = do
  names <- getDirectoryContents topdir
  let properNames = filter (`notElem` [".", "..",".refactor"]) names
  paths <- forM properNames $ \name -> do
    let path = topdir </> name
    isDirectory <- doesDirectoryExist path
    if isDirectory
      then getRecursiveContents path
      else if isInfixOf ".php" path 
      		  then return [path]
      		  else return []
  return (concat paths)